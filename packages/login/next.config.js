const assetPrefix = process.env.BUILDING_FOR_NOW ? '/login' : '';

module.exports = {
  assetPrefix
  , env: {
    ASSET_PREFIX: assetPrefix
  }
}
