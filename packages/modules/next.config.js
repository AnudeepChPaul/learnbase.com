const assetPrefix = process.env.BUILDING_FOR_NOW ? '/modules' : ''

module.exports = {
  assetPrefix
  , env: {
    ASSET_PREFIX: assetPrefix
  }
}
